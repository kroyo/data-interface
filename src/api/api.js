import axios from 'axios';
const baseURL = process.env.NODE_ENV === 'development' ? '/api' : '';
const baseURL2 = process.env.NODE_ENV === 'development' ? ' https://www.easy-mock.com/mock/5b1e45f84b37346ccb3044b7/datashow' : '';

export const tableApi = (tableName) => {return axios.post(baseURL+'/console/data/resources/stat/getTravelAndWBDate.do?tableNames=' + tableName).then((res) => res)}
export const resourceApi = () => {return axios.post(baseURL + '/console/data/resources/stat/getSysTime.do').then((res)=>res)};
export const kakouApi = () => {return axios.post(baseURL2+'/kingyea-dataservice-web/data/query/22').then((res)=>res)}

//旅客分析页面
// export const bustickeApi = () => {}
// kingyea-data-service-web/data/query/362360
//
// /kingyea-dataservice-web/data/query/22

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuex from 'vuex'
import axios from 'axios'
import {Table,TableColumn,Radio,RadioGroup,RadioButton} from 'element-ui';
// 引入 ECharts 主模块
let echarts = require('echarts/lib/echarts');
// let echarts = require('echarts');
import 'echarts-gl';
import "babel-polyfill"

// 引入柱状图,饼状图
require('echarts/lib/chart/bar');
require('echarts/lib/chart/pie');
require("echarts/lib/chart/map");
// 引入提示框和标题组件
require('echarts/lib/component/tooltip');
require('echarts/lib/component/title');
require("echarts/lib/component/markPoint");
require("echarts/lib/component/legendScroll");
require("echarts/lib/component/geo");
require("echarts/map/js/china")

import 'element-ui/lib/theme-chalk/index.css';
Vue.config.productionTip = false

Vue.use(Table);
Vue.use(TableColumn);
Vue.use(Radio);
Vue.use(RadioGroup);
Vue.use(RadioButton);

Vue.prototype.$echarts = echarts;
Vue.prototype.$axios = axios;

//改写date内置方法
Date.prototype.toLocaleString = function() {
        return (this.getMonth() + 1) + "-" + this.getDate();
    };
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
import './util/tongji'

import Vue from 'vue'
import Router from 'vue-router'
import navPage from '@/views/navPage.vue'
import dataServicePage from '@/views/dataServicePage'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'nav',
      component: navPage
    },
    {
      path: '/home',
      name: 'home',
      component: () => import('@/views/homePage.vue')
    },
    {
      path: '/travel',
      name: 'travel',
      component: () => import('@/views/travelPage.vue')
    },
    {
      path: '/dataService',
      name: 'dataService',
      component: dataServicePage
    }
  ]
})

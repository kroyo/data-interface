//字符串换行（用于Echart)
export function newline(params,length){
  var newParamsName = "";// 最终拼接成的字符串
  var paramsNameNumber = params.length;// 实际标签的个数
  var provideNumber = length;// 每行能显示的字的个数
  var rowNumber = Math.ceil(paramsNameNumber / provideNumber);// 换行的话，需要显示几行，向上取整
  /**
   * 判断标签的个数是否大于规定的个数， 如果大于，则进行换行处理 如果不大于，即等于或小于，就返回原标签
   */
  // 条件等同于rowNumber>1
  if (paramsNameNumber > provideNumber) {
      /** 循环每一行,p表示行 */
      for (var p = 0; p < rowNumber; p++) {
          var tempStr = "";// 表示每一次截取的字符串
          var start = p * provideNumber;// 开始截取的位置
          var end = start + provideNumber;// 结束截取的位置
          // 此处特殊处理最后一行的索引值
          if (p == rowNumber - 1) {
              // 最后一次不换行
              tempStr = params.substring(start, paramsNameNumber);
          } else {
              // 每一次拼接字符串并换行
              tempStr = params.substring(start, end) + "\n";
          }
          newParamsName += tempStr;// 最终拼成的字符串
      }

  } else {
      // 将旧标签的值赋给新标签
      newParamsName = params;
  }
  //将最终的字符串返回
  return newParamsName;
}
//数字每三位增加逗号
export function formatNum(n){
   var b=parseInt(n).toString();
   var len=b.length;
   if(len<=3){return b;}
   var r=len%3;
   return r>0?b.slice(0,r)+","+b.slice(r,len).match(/\d{3}/g).join(","):b.slice(r,len).match(/\d{3}/g).join(",");
 }
//数组切块
export const chunk = (arr, size) =>
  Array.from({length: Math.ceil(arr.length / size)}, (v, i) => arr.slice(i * size, i * size + size));
